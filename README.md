## Axib-task
Choose a self-explaining name for your project.

## Description
Simple Vue app, that connects to flask as backend service and shows pong if everything work fine

## Installation
#### Docker installation

Update the apt package index and install packages to allow apt to use a repository over HTTPS:
```bash
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
```

Add Docker’s official GPG key:
```bash
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```
Use the following command to set up the repository:
```bash
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
### Install Docker Engine

Update the apt package index:
`sudo apt-get update`

Install Docker Engine, containerd, and Docker Compose.
`sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin`

Verify that the Docker Engine installation is successful by running the hello-world image.
`sudo docker run hello-world`


### Fix container network MTU

In some cases, docker network MTU is higher than on host adapters. This difference could make network issues when you will try to update/install packages (on Alpine especially)
so we need fix that

First of all shutdown docker
`systemctl stop docker.socket && systemctl stop docker`

Write to docker special config
`nano /etc/docker/daemon.json`

write to that file next text
```json
{
    "exec-opts": ["native.cgroupdriver=systemd"],

    "log-driver": "json-file",
    "log-opts": {
      "max-size": "10m",
      "max-file": "3"
    },

    "mtu": 1399
}
```

Now we can start docker
`systemctl start docker`
docker.socket will up automaticly

Remove default docker container network
`docker network rm docker_gwbridge`

Make new network with fixed parameters of MTU and default parameters
`docker network create --opt com.docker.network.bridge.name=docker_gwbridge --opt com.docker.network.bridge.enable_icc=false --opt com.docker.network.bridge.enable_ip_masquerade=true --opt com.ocker.network.driver.mtu=1399 docker_gwbridge`

### Switch to docker swarm mode

Run to create a single-node swarm on the current node.
`docker swarm init`

## Install needed docker stacks
Those steps no need for local development (just move to usage if you plan run that project localy)

Copy folders from prepare to server in folder /opt

### Traefik install
Note: Those instalation of traefik isn't full, this step is different between local and dev (at least SSL settings)
This step should be done smth like Ansible

Make new network for link Traefik and docker containers:
`docker network create --attachable --scope swarm --driver overlay --opt com.docker.network.driver.mtu=1399 ingress_external`

Deploy stack with Traefik:
`docker stack deploy ingress -c /opt/traefik/docker-compose.yml`

to check Traefik dashboard use that
```
URL: http://dashboard.loc
traefik dashboard login
username: admin
password: xo6ud5OhXee7Iuqu
```

### Remoteapi install
Move to directory with remoteapi files:
`cd /opt/remoteapi`

replace {$DEV_DOMAIN} with REAL dev domain:
`nano ./docker-compose.yml`
```yaml
...
    ports:
     - 2317:443
    environment:
     - CREATE_CERTS_WITH_PW=thi8uj1ruojeiXie6ta2ohjairaic6eg
     - CERT_HOSTNAME={$DEV_DOMAIN}
    volumes:
     - ./certs:/data/certs
...
```

Now build remoteapi image:
`docker build -f ./Dockerfile -tremoteapi_remote-api .`

Deploy stack with remoteapi
`docker stack deploy remoteapi -c /opt/remoteapi/docker-compose.yml`

## Verify stack instalations

To check that all stack are runned use:
`docker service ls`

Example:
```
# docker service ls
ID             NAME                   MODE         REPLICAS   IMAGE                         PORTS
yzx2h3q7dftw   ingress_app            replicated   1/1        traefik:v2.10
kcz50jyu7uzz   remoteapi_remote-api   replicated   1/1        remoteapi_remote-api:latest   *:2317->443/tcp
```

## Usage
### Run locally
Clone repository
`git clone https://gitlab.com/Enemon/axib-task-frontend.git`

move to project directory
`cd ./axib-task-frontend`

build docker image
`docker build -f .deploy/Dockerfile.local -t vuejs-app ./client`

Deploy docker stack (with local settings)
`docker stack deploy -c ./.deploy/docker-compose.yml -c ./.deploy/docker-compose-local.yml test-app`
docker-compose-local.yml contain settings for local development

move to http://localhost:5173
if backend is up, you will see button with text "pong"

### Using CI/CD

First of all, make sure that you done all preparation steps on target server

Move to remoteapi certificates folder
`cd /opt/remoteapi/certs/client`

Now we need export those certs to Gitlab variables in base64
`cat ca.pem | base64 -w0`
`cat cert.pem | base64 -w0`
`cat key.pem | base64 -w0`

Add output of those three commands to separated group/repository variables

TLSCACERT_DEV_SERVER - `cat ca.pem | base64 -w0` 
TLSCERT_DEV_SERVER - `cat cert.pem | base64 -w0`
TLSKEY_DEV_SERVER - `cat key.pem | base64 -w0`

After we need change DOCKER_HOST variable in deploy step, to real server domain
```yaml
  image: docker:20.10.0
  variables:
    DOCKER_HOST: tcp://app.loc:2317
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "/certs"
  before_script:
    - *before_s
```
`tcp://...:2317` - TCP protocol and port must be defined here

Now we can commit and push in main branch, CI/CD will start automaticly (we could also make different rules for build/deploy on separated branches)
## Author
Nemokaiev Dmytro, @enemon

